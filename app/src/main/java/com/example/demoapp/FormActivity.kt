package com.example.demoapp

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.RadioGroup
import android.widget.Spinner
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_form.*
import java.time.LocalDate
import java.time.Period


class FormActivity: AppCompatActivity() {

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    private val spinnerYearItems = arrayListOf("")
    private val spinnerMonthItems = arrayListOf("")
    private val spinnerDateItems = arrayListOf("")
    var gender: String = ""
    var year: String = "0"
    var month: String = "0"
    var date: String = "0"


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form)
        supportActionBar?.title = "アンケート"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val radioGroup: RadioGroup = findViewById(R.id.radiogroup)
        radioGroup.setOnCheckedChangeListener {_, checkedId: Int ->
            when(checkedId) {
                R.id.radio_man -> gender = "男性"
                R.id.radio_woman -> gender = "女性"
                R.id.radio_other -> gender = "その他"
            }
        }

        val yearspinner = findViewById<Spinner>(R.id.spinner_year)
        val yaeradapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, spinnerYearItems)
        yaeradapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        yearspinner.adapter = yaeradapter

        for (i in 1950..2020) {
            spinnerYearItems.add("$i")
        }

        val monthspinner = findViewById<Spinner>(R.id.spinner_month)
        val monthadapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, spinnerMonthItems)
        monthadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        monthspinner.adapter = monthadapter

        for (i in 1..12) {
            spinnerMonthItems.add("$i")
        }

        val datespinner = findViewById<Spinner>(R.id.spinner_date)
        val dateadapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, spinnerDateItems)
        dateadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        datespinner.adapter = dateadapter

        for (i in 1..31) {
            spinnerDateItems.add("$i")
        }

        val btnIntent = findViewById<Button>(R.id.button)
        btnIntent.setOnClickListener {
            val intent = Intent(this@FormActivity, ResultFormActivity::class.java)
            val name = editText.text.toString()
            val birthday: String = year + "年" + month + "月" + date + "日"
            val today = LocalDate.now()
            val age = Period.between(LocalDate.of(year.toInt(),month.toInt(),date.toInt()), today).years.toString()

            intent.putExtra("Name", name)
            intent.putExtra("Gender", gender)
            intent.putExtra("Birthday", birthday)
            intent.putExtra("Age", age)

            startActivity(intent)
        }
    }
}
